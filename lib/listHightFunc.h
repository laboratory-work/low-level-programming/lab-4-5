#ifndef LAB_4_LISTHIGHTFUNC_H
#define LAB_4_LISTHIGHTFUNC_H

#include "linkedlist.h"

/**
 * Accepts a pointer to the list start and a function (which returns void and accepts an int).
 * It launches the function on each element of the list.
 * @param root - pointer to the list
 * @param func - function (which returns void and accepts an int)
 */
void foreach(struct node* root, void(*func)(int));

/**
 * Does the same but changes the source list.
 * @param root - pointer to the list
 * @param func - function (which returns int and accepts an int)
 */
void map_mut(struct node* root, int(*func)(int));

/**
 * Accepts a function f and a list. It returns a new list containing the results of
 * the func applied to all elements of the source list. The source list is not affected.
 * @param root - pointer to the list
 * @param func - function (which returns int and accepts an int)
 * @return A new list containing the results of the func applied to all elements of the source list
 */
struct node* map(struct node* root, int(*func)(int));

/**
 * Accepts the initial value s, list length n, and function f.
 * It then generates a list of length n as follows
 * @param first - initial value
 * @param length - list length
 * @param func - function (which returns int and accepts an int)
 * @return A new list with generate
 */
struct node* iterate(int first, int length, int(*func)(int));

/**
 * Is a bit more complicated
 * @param root - pointer to the list
 * @param accum - The accumulator starting value.
 * @param func - A function func(x, a).
 * @return It returns a value of the same type as the accumulator, computed in the following way.
 */
int foldl(struct node* root, int accum, int(*func)(int, int));

#endif //LAB_4_LISTHIGHTFUNC_H
