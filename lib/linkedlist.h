#ifndef LAB_4_LINKEDLIST_H
#define LAB_4_LINKEDLIST_H

#include <stddef.h>

struct node{
    int value;
    struct node* next;
} ;

/**
 * Accepts a number, returns a pointer to the new linked list node.
 * @param value - accepts a number
 * @return returns a pointer to the new linked list node.
 */
struct node* list_create(int value);

/**
 * Accepts a number and a pointer to a pointer to the linked list.
 * Prepends the new node with a number to the list.
 * @param root - pointer to a pointer to the linked list
 * @param value - accepts a number
 * @return pointer with addition node to the linked list
 */
struct node* list_add_front(struct node* root, int value);

/**
 * Adds an element to the end of the list. The signature is the same as @a list_add_front.
 * @param root - pointer to a pointer to the linked list
 * @param value - accepts a number
 * @return NULL
 */
struct node* list_add_back(struct node* root, int value);

/**
 * Gets an element by index, or returns 0 if the index is outside the list bounds.
 * @param root - pointer to a pointer to the linked list
 * @param index - index
 * @return element by index
 */
int list_get(struct node* root, size_t index);

/**
 * Frees the memory allocated to all elements of list.
 * @param root - pointer to a pointer to the linked list
 */
void list_free(struct node* root);

/**
 * Accepts a list and computes its length.
 * @param root - pointer to a pointer to the linked list
 * @return size of linked list
 */
size_t list_length(struct node* root);

/**
 * Accepts a list and an index, returns a pointer to struct list, corresponding to the node at this index.
 * If the index is too big, returns NULL.
 * @param root - pointer to a pointer to the linked list
 * @param index index
 * @return returns a pointer to struct list. If the index is too big, returns NULL.
 */
struct node* list_node_at(struct node* root, size_t index);

/**
 * Accepts a list, returns the sum of elements.
 * @param root - pointer to a pointer to the linked list
 * @return sum of elements
 */
long list_sum(struct node* root);

/**
 * Print all linked list
 * @param root - pointer to a pointer to the linked list
 */
void list_print(struct node* root);

#endif //LAB_4_LINKEDLIST_H
