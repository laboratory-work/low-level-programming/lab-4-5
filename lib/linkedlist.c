#include <stdlib.h>
#include <stdio.h>
#include "linkedlist.h"

const size_t sizeList = sizeof(struct node);

struct node* list_create(int value){
    return list_add_front(NULL, value);
}

struct node* list_add_front(struct node* root, int value){
    struct node* new_node = (struct node*)malloc(sizeList);
    new_node->next = root;
    new_node->value = value;
    return new_node;
}

struct node* list_add_back(struct node* root, int value){
    struct node* new_node;

    while (root->next != NULL){
        root = root->next;
    }

    new_node = (struct node*)malloc(sizeList);
    new_node->value = value;
    new_node->next = NULL;
    root->next = new_node;

    return root;
}

int list_get(struct node* root, size_t index){
    struct node* element = list_node_at(root, index);
    if(element == NULL) {
        return 0;
    } else {
        return element->value;
    }
}

void list_free(struct node* root){
    struct node* forFree;
    while(root != NULL){
        forFree = root;
        root = root->next;
        free(forFree);
    }
}

size_t list_length(struct node* root){
    size_t size = 0;
    while (root != NULL) {
        size++;
        root = root->next;
    }
    return size;
}

struct node* list_node_at(struct node* root, size_t index){
    size_t i = 0;
    while(i < index && root != NULL){
        root = root->next;
        i++;
    }

    if(index == i){
        return root;
    }else{
        return NULL;
    }
}

long list_sum(struct node* root){
    long sum = 0;
    while (root != NULL){
        sum += root->value;
        root = root->next;
    }

    return sum;
}


void list_print(struct node* root) {
    if(root == NULL) return;
    printf(" > %d", root->value);
    list_print(root->next);
}