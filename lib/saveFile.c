#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "saveFile.h"

#define null NULL

bool save(struct node* root, const char* filename) {
    FILE *file = fopen(filename, "w");
    if(!file) return false;

    while(root != null) {
        if(fwrite(&(root->value), sizeof(int), 1, file) != 1) {
            fclose(file);
            return false;
        }
        root = root->next;
    }
    if(fclose(file) == EOF) {
        return false;
    }

    return true;
}

bool load(struct node** root, const char* filename){
    FILE *file = fopen(filename, "r");
    if(!file) return false;

    int value;
    if(fread(&value, sizeof(int), 1, file)!=1){
        fclose(file);
        return false;
    }

    struct node* new_list = NULL;
    new_list = list_create(value);

    while(fread(&value, sizeof(int), 1, file) == 1) {
        list_add_front(new_list, value);
    }

    list_free(*root);
    *root = new_list;

    if(fclose(file) == EOF)
        return false;
    return true;

}

bool serialize(struct node* root, const char* filename){
    FILE *file = fopen(filename, "wb");
    if(!file) return false;

    while(root != NULL) {
        if(fwrite(&(root->value), sizeof(int), 1, file) != 1) {
            fclose(file);
            return false;
        }
        root = root->next;
    }
    if(fclose(file) == EOF)
        return false;

    return true;
}

bool deserialize(struct node** root, const char* filename){
    FILE *file = fopen(filename, "rb");
    if(!file) return false;

    int value;
    if(fread(&value, sizeof(int), 1, file)!=1){
        fclose(file);
        return false;
    }
    struct node* new_head = NULL;
    new_head = list_create(value);
    while(fread(&value, sizeof(int), 1, file) == 1) {
        list_add_front(new_head, value);
    }
    list_free(*root);
    *root = new_head;

    if(fclose(file) == EOF)
        return false;

    return true;
}