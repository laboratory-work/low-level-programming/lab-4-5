#include "listHightFunc.h"
#define null NULL

void foreach(struct node* root, void(*func)(int)) {
    while(root != null) {
        func(root->value);
        root = root->next;
    }
}

void map_mut(struct node* root, int(*func)(int)) {
    while (root != null) {
        root->value = func(root->value);
        root = root->next;
    }
}

struct node* map(struct node* root, int(*func)(int)) {
    if(root->next != null) {
        return list_add_front(map(root->next, func), func(root->value));
    }else {
        return list_create(func(root->value));
    }
}

struct node* iterate(int first, int length, int(*func)(int)) {
    struct node *new_list = list_create(first);
    for(int i = 0; i < length; i++) {
        new_list = list_add_front(new_list, func(new_list->value));
    }
    return new_list;
}

int foldl(struct node* root, int accum, int(*func)(int, int)) {
    while (root != null) {
        accum = func(accum, root->value);
        root = root->next;
    }
    return accum;
}