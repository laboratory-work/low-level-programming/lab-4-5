#ifndef LAB_4_SAVEFILE_H
#define LAB_4_SAVEFILE_H

#include <stdbool.h>
#include "linkedlist.h"

bool save(struct node* root, const char* filename);
bool load(struct node** root, const char* filename);
bool serialize(struct node* root, const char* filename);
bool deserialize(struct node** root, const char* filename);

#endif //LAB_4_SAVEFILE_H
