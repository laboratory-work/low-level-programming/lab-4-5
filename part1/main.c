#include <stdio.h>
#include "../lib/linkedlist.h"

int main(int argc, char **argv) {
    int n = 0;
    struct node* root = NULL;

    printf("Enter the elements (0 - is end): ");

    while ((n = getchar()) != EOF) {
        n = n - '0';
        if(n == 0) break;
        if(n > 0 && n < 10) {
            if (root == NULL) root = list_create(n);
            else root = list_add_front(root, n);
        }
    }

    printf("\nPrint list: "); list_print(root); printf("\n");
    printf("Sum of elements: %ld\n", list_sum(root));
    printf("Count of elements: %ld\n", list_length(root));

    printf("Enter the number of element: ");
    scanf("%d", &n);
    int val = list_get(root, n);
    if (val) {
        printf("The value at index %d element is: %d\n", n, val);
    } else {
        printf("Not enough length\n");
    }

    list_free(root);

    return 0;
}
