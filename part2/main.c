#include <stdio.h>
#include <math.h>
#include <limits.h>
#include "../lib/linkedlist.h"
#include "../lib/listHightFunc.h"

int square(int n);
int mul_2(int n);
int mul_3(int n);
int sum(int a, int b);

int square(int n) {
    if (n < sqrt(INT_MAX))
        return n * n;
    else
        return 0;
}
int mul_2(int n) {
    if (n < INT_MAX / 2)
        return n * 2;
    else
        return 0;
}
int mul_3(int n) {
    if (n < INT_MAX / 3)
        return n * 3;
    else
        return 0;
}
int sum(int a, int b) {
    if (a < INT_MAX - b)
        return a + b;
    else
        return 0;
}

void printspace(int value) {
    printf("%d ", value);
}
void printnewline(int value) {
    printf("%d\n", value);
}

int main(int argc, char **argv) {
    int n;
    struct node* root;
    printf("Enter the elements (0 - is end): ");
    while ((n = getchar()) != EOF) {
        n = n - '0';
        if(n == 0) break;
        if(n > 0 && n < 10) {
            if (root == NULL) root = list_create(n);
            else root = list_add_front(root, n);
        }
    }

    printf("List: ");
    foreach(root, &printspace);
    printf("\n");
    foreach(root, &printnewline);
    printf("\n");

    struct node* modif_list;

    printf("Square\n");
    list_print(modif_list = map(root, &square));
    list_free(modif_list);
    printf("\n");

    printf("Foldl: %d\n", foldl(root, 0, &sum));

    printf("Mul 3 - \n");
    map_mut(root, &mul_3);
    list_print(root);
    printf("\n");

    printf("Iterations\n");
    list_print(iterate(2, 10, &mul_2));
    printf("\n");

    list_free(modif_list);
    list_free(root);

    return 0;
}